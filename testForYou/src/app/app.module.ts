import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import{ Routes, RouterModule } from '@angular/router';
import { HowWorksStudentsComponent } from './how-works-students/how-works-students.component';
import { HowWorksInstitutionsComponent } from './how-works-institutions/how-works-institutions.component';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { GenInstructionsComponent } from './gen-instructions/gen-instructions.component';


const routes: Routes =[
//  { path: '', component:HeaderComponent}
  { path:'', component:HeaderComponent,
  children:[
    {path:"how-it-works-institution", component:HowWorksInstitutionsComponent,outlet:'route1'},
    {path:"how-it-works-Students", component:HowWorksStudentsComponent, outlet:'route2'},
  ] },
  { path:"test", component:GenInstructionsComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FeedbackComponent,
    HowWorksStudentsComponent,
    HowWorksInstitutionsComponent,
    GenInstructionsComponent

  ],
  imports: [
    BrowserModule,
    MDBBootstrapModule.forRoot(),
    RouterModule.forRoot(routes),
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
