import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenInstructionsComponent } from './gen-instructions.component';

describe('GenInstructionsComponent', () => {
  let component: GenInstructionsComponent;
  let fixture: ComponentFixture<GenInstructionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenInstructionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenInstructionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
