import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HowWorksInstitutionsComponent } from './how-works-institutions.component';

describe('HowWorksInstitutionsComponent', () => {
  let component: HowWorksInstitutionsComponent;
  let fixture: ComponentFixture<HowWorksInstitutionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HowWorksInstitutionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HowWorksInstitutionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
