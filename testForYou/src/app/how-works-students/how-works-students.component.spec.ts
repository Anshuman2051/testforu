import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HowWorksStudentsComponent } from './how-works-students.component';

describe('HowWorksStudentsComponent', () => {
  let component: HowWorksStudentsComponent;
  let fixture: ComponentFixture<HowWorksStudentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HowWorksStudentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HowWorksStudentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
